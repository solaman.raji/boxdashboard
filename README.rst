==============
Box Dashboard
==============

Installation
-------------

Install vagrant

    vagrantup.com

Install VirtualBox

    virtualbox.org

Add your ssh-keys to agent
::
    eval `ssh-agent -s`
    ssh-add ~/.ssh/id_rsa
    # Or any other ssh key that you use to access git

Make sure that you have clone boxdashboard and box-fronten under the same parent directory.
::
    git clone git@bitbucket.org:alemhealthrepo/boxdashboard.git
    git clone git@bitbucket.org:alemhealthrepo/box-frontend.git

Run vagrant
::
    cd boxdashboard
    cd vagrant
    vagrant up

Check if the dashboard frontend is running properly

    http://192.168.11.13/dashboard/
    http://192.168.11.13/dashboard-api/

Details please check the repository wiki and confluence.
