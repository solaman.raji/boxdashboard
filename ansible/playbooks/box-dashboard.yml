---
- hosts: "{{ the_host }}"
  gather_facts: False
  become: true
  tasks:
  - name: "FIX: Ubuntu 16.04 LTS doesn't come with certain modules, required by ansible"
    raw: test -e /usr/bin/python || (apt -y update && apt install -y python-minimal)

- name: Deploy Box Dashboard
  hosts: "{{ the_host }}"
  gather_facts: Yes
  vars:
    application_parent: "{{ ansible_env.HOME }}"
    user: "{{ ansible_ssh_user }}"
    www_group: www-data
    proj_name: boxdashboard
    venv_home: "{{ ansible_env.HOME }}/.virtualenvs"
    venv_path: "{{ venv_home }}/{{ proj_name }}"
    proj_dirname: "{{ proj_name }}"
    proj_path: "{{ application_parent }}/{{ proj_dirname }}"
    app_path: "{{ proj_path }}/{{ proj_name }}"
    www_root: /var/www/dashboard
    www_dashboard_static: /var/www/dashboard-static/
    www_grav_root: /var/www/gravity
    reqs_path: "{{ proj_path }}/requirements.txt"
    manage: "{{ python }} {{ proj_path }}/manage.py"
#    live_hostname: 192.168.11.13.xip.io
    live_hostname: "{{ ansible_eth1.ipv4.address }}.xip.io"
    domains:
        - dev-box-dashboard-01.cloud.alemhealth.com
        - dev-box-dashboard-02.cloud.alemhealth.com
        - test-hub-dashboard-01.cloud.alemhealth.com
        - dev.13.127.4.121.xip.io
        - www.13.126.141.238.xip.io
        - 192.168.11.13
#        - "{{ ansible_eth1.ipv4.address }}.xip.io"
#        - "www.{{ ansible_eth1.ipv4.address }}.xip.io"
    directories:
        - /var/log/boxdashboard/
        - /var/log/boxdashboard/gunicorn
        - /var/log/boxdashboard/consumer
        - /var/lib/boxdashboard/
        - /var/lib/boxdashboard/data
    web_directories:
        - "{{ www_root }}"
        - "{{ www_dashboard_static }}"
        - "{{ www_grav_root }}"

    repo_url: git@bitbucket.org:alemhealthrepo/boxdashboard.git
    hub_http: 88
    uwsgi_port: 8000
    gunicorn_port: 8088
    gravity_port: 8008
    locale: en_US.UTF-8

    conf_path: /etc/nginx/conf
    tls_enabled: False
    python: "{{ venv_path }}/bin/python"
    database_name: "{{ proj_name }}"
    database_user: "alemhealth"
    database_host: localhost
    database_port: 5432
    gunicorn_proc_name: box-dashboard
  vars_files:
    - secrets.yml

  tasks:
    - name: Show hostnames
      debug:
        msg: "System {{ inventory_hostname }} has uuid {{ ansible_product_uuid }} and hostname {{ ansible_hostname }}, {{ ansible_nodename }} "
    - name: Show ip address of the host
      debug:
        msg: "System has IP address {{ ansible_default_ipv4.address }}, {{ ansible_all_ipv4_addresses }}"
    - name: Show eth0 ip address
      debug:
        msg: "eth0 ip address: {{ ansible_eth0.ipv4.address }}"
      when: ansible_eth0 is defined
    - name: Show application parent directory name
      debug:
        msg: "Home directory: {{ ansible_env.HOME }}, application parent is '{{ application_parent }}' and project path {{ proj_path }}"



    - name: install apt packages
      apt: pkg={{ item }} update_cache=yes cache_valid_time=3600
      become: True
      with_items:
        - git
        - nginx
        - python-dev
        - python-pip
        - python-setuptools
        - python-virtualenv
        - supervisor
    - name: check out the repository on the host
      git: repo={{ repo_url }} dest={{ proj_path }} accept_hostkey=yes refspec=refs/heads/master:refs/remotes/origin/master
      notify: restart supervisor
      when: the_host != 'localhost'

    - name: create directories if it doesn't exist
      become: True
      file:
        state: directory
        mode: u=rwx,g=rx,o=rx
        owner: "{{ user }}"
        group: "{{ user }}"
        path: "{{ item }}"
        recurse: yes
      with_items:
        - "{{ directories }}"
        - "{{ web_directories }}"
    - name: Add user to secondary group
      become: True
      user:
        name: "{{ user }}"
        groups: "{{ www_group }}"
        append: yes

#    - name: Copy dashboard frontend files
#      command: "cp -rv /box-dashboard-frontend/static/ {{ www_root_dir }}/"

    - name: Add alias to activate virtualenv
      lineinfile:
        path: "{{ ansible_env.HOME }}/.bashrc"
        regexp: 'venv-boxdashboard='
        insertafter: EOF
        line: 'alias venv-boxdashboard=". {{ venv_path }}/bin/activate"'
        state: present
        create: yes
    - name: Add alias to activate virtualenv of Hub
      lineinfile:
        path: "{{ ansible_env.HOME }}/.bashrc"
        regexp: 'venv-hub='
        insertafter: EOF
        line: 'alias venv-hub=". ~/virtual-env/alemhealthhub/bin/activate"'
        state: present
        create: yes
    - name: install required python packages
      pip: name={{ item }} virtualenv={{ venv_path }}
      with_items:
        - gunicorn
        - setproctitle
        - virtualenvwrapper
    - name: install requirements.txt
      pip: requirements={{ reqs_path }} virtualenv={{ venv_path }}
#    - name: install box dashboard
#      pip: name: file:///path/to/MyApp.tar.gz

    - name: generate the settings file
      template: src=templates/local_settings.py.j2 dest={{ app_path }}/local_settings.py

    - name: sync the database, apply migrations, collect static content
      django_manage:
        command: "{{ item }}"
        app_path: "{{ proj_path }}"
        virtualenv: "{{ venv_path }}"
      with_items:
#        - syncdb
        - migrate
        - collectstatic

#    - name: set the site id
#      script: scripts/setsite.py
#      environment:
#        PATH: "{{ venv_path }}/bin"
#        PROJECT_DIR: "{{ proj_path }}"
#        WEBSITE_DOMAIN: "{{ live_hostname }}"
#    - name: set the admin password
#      script: scripts/setadmin.py
#      environment:
#        PATH: "{{ venv_path }}/bin"
#        PROJECT_DIR: "{{ proj_path }}"
#        ADMIN_PASSWORD: "{{ admin_pass }}"

    - name: set the gunicorn config file
      template: src=templates/gunicorn.conf.py.j2 dest={{ proj_path }}/gunicorn.conf.py
    - name: set the supervisor gunicorn config file
      template: src=templates/gunicorn.conf.j2 dest=/etc/supervisor/conf.d/gunicorn-{{ proj_name }}.conf
      become: True
      notify: restart supervisor
    - name: set the supervisor adtivity listener config file
      template: src=templates/activity-listener.conf.j2 dest=/etc/supervisor/conf.d/activity-listener-{{ proj_name }}.conf
      become: True
      notify: restart supervisor
    - name: set the nginx config file
      template: src=templates/nginx.conf.j2 dest=/etc/nginx/sites-available/dashboard
      notify: restart nginx
      become: True
    - name: enable the nginx config file
      file:
        src: /etc/nginx/sites-available/dashboard
        dest: /etc/nginx/sites-enabled/dashboard
        state: link
      notify: restart nginx
      become: True

    - name: set the hub nginx config file
      template: src=templates/nginx-hub.conf.j2 dest=/etc/nginx/sites-available/hub
      notify: restart nginx
      become: True
    - name: enable the hub nginx config file
      file:
        src: /etc/nginx/sites-available/hub
        dest: /etc/nginx/sites-enabled/hub
        state: link
      notify: restart nginx
      become: True

    - name: set the Gravity nginx config file
      template: src=templates/nginx-gravity.conf.j2 dest=/etc/nginx/sites-available/grav
      notify: restart nginx
      become: True
    - name: enable the Gravity nginx config file
      file:
        src: /etc/nginx/sites-available/grav
        dest: /etc/nginx/sites-enabled/grav
        state: link
      notify: restart nginx
      become: True

    - name: remove the default nginx config file
      file: path=/etc/nginx/sites-enabled/default state=absent
      notify: restart nginx
      become: True
    - name: ensure config path exists
      file: path={{ conf_path }} state=directory
      become: True
      when: tls_enabled
    - name: create ssl certificates
      command: >
        openssl req -new -x509 -nodes -out {{ proj_name }}.crt
        -keyout {{ proj_name }}.key -subj '/CN={{ domains[0] }}' -days 3650
        chdir={{ conf_path }}
        creates={{ conf_path }}/{{ proj_name }}.crt
      become: True
      when: tls_enabled
      notify: restart nginx
#    - name: install poll  cron job
#      cron: name="poll twitter" minute="*/5" user={{ user }} job="{{ manage }} poll_twitter"
  post_tasks:
    - name: notify Slack that the servers have been updated
      local_action: >
        slack
        domain=acme.slack.com
        token={{ slack_token }}
        msg="web server {{ inventory_hostname }} configured"
      when: slack_token is defined

  handlers:
    - name: restart supervisor
      supervisorctl: name=gunicorn-{{ proj_name }} state=restarted
      become: True
      listen: restart supervisor

    - name: restart activity listener
      supervisorctl: name=activity-listener-{{ proj_name }} state=restarted
      become: True
      listen: restart supervisor

    - name: restart nginx
      service: name=nginx state=restarted
      become: True
