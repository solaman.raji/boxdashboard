#!/bin/bash
echo "Update aptitude packages"
apt update
echo "Install python and other packages"
apt install -y python2.7-dev python-pip tmux curl htop iftop swapspace

apt install -y rabbitmq-server

pip install -U pip
echo "Install python virtual environment"
pip install virtualenv virtualenvwrapper

# Frontend packages
echo "Remove node and nodejs packages"
apt-get --purge remove node
apt-get --purge remove nodejs

echo "Install nodejs 8.x"
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
apt-get install -y nodejs

ln -s /usr/bin/nodejs /usr/bin/node

echo "Create ansible config file"
mkdir -p /etc/ansible
echo "localhost ansible_connection=local" > /etc/ansible/hosts
