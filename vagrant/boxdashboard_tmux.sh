#!/bin/sh

SESSION_NAME="v_boxdashboard"
cd /boxdashboard
tmux new -s $SESSION_NAME -d

tmux rename-window -t $SESSION_NAME:0 "htop"
tmux send-keys -t $SESSION_NAME:0 "htop" C-m

tmux new-window -t $SESSION_NAME:1 -n "django-server"
tmux send-keys -t $SESSION_NAME:1 "python manage.py runserver 192.168.11.13:8000" C-m

tmux new-window -t $SESSION_NAME:2 -n "boxdashboard.log"
tmux send-keys -t $SESSION_NAME:2 "tail -f /var/log/boxdashboard/dashboard.log" C-m

tmux new-window -t $SESSION_NAME:3 -n "database"

tmux new-window -t $SESSION_NAME:4 -n "bash"

tmux attach -t $SESSION_NAME
