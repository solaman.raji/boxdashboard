#!/bin/bash

export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

echo "Create virtual environment for ansible"
mkvirtualenv virtualenv-deployment
pip install ansible
workon virtualenv-deployment

echo "Change to project directory"
export PROJECT=boxdashboard
export PROJECT_HOME=/$PROJECT
cd $PROJECT_HOME

echo "Run ansible playbook"
export PLAYBOOKS_HOME=$PROJECT_HOME/ansible/playbooks

echo "ping localhost using ansible"
ansible localhost -m ping -vvvv

echo "Run playbook for boxdashboard"
echo ""
ansible-playbook $PLAYBOOKS_HOME/box-dashboard.yml -e the_host=localhost -v -e application_parent=

echo "Change to frontend project"
cd /box-frontend

# Adapting from https://docs.npmjs.com/getting-started/fixing-npm-permissions
echo "Change npm's default directory ======= "
echo `npm config get prefix`

NPM_GLOBAL_DIR=~/.npm-global
echo "Make a directory for global installations:"
if [ ! -d "$NPM_GLOBAL_DIR" ]; then
    mkdir $NPM_GLOBAL_DIR
fi

echo "Configure npm to use the new directory path:"
npm config set prefix "$NPM_GLOBAL_DIR"

echo "Open or create a ~/.profile file and add this line"
echo "export PATH=$NPM_GLOBAL_DIR/bin:"'$PATH' >> ~/.bashrc

echo "Back on the command line, update your system variables:"
source ~/.profile

echo "Instead of steps 2-4, you can use the corresponding ENV variable (e.g. if you don't want to modify ~/.profile):"
export NPM_CONFIG_PREFIX=$NPM_GLOBAL_DIR

echo "Test: Download a package globally without using sudo."
npm install -g gulp-cli

#  npm uninstall --global gulp gulp-cli
# rm /usr/local/share/man/man1/gulp.1
# npm install --global gulp-cli

echo "export NPM_CONFIG_PREFIX=$NPM_GLOBAL_DIR" >> ~/.bashrc

echo "npm's default directory is changed to $NPM_GLOBAL_DIR ============ "

echo " Build frontend project"
. ah_build.sh

# Run ansible playbook for frontend
export PLAYBOOK=$PLAYBOOKS_HOME/box-dashboard-frontend.yml

ansible-playbook -e the_host=localhost $PLAYBOOKS_HOME/box-dashboard-frontend.yml  -v
#ansible-playbook -i $ANSIBLE_HOSTS_FILE $PLAYBOOK -v
