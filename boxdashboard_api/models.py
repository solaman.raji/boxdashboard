from __future__ import unicode_literals

import datetime
import uuid

from django.db import models
from django.utils import timezone
from model_utils import Choices
from model_utils.fields import StatusField


class Study(models.Model):
    STATUS = Choices('active', 'inactive')

    study_id = models.IntegerField(unique=True)
    guid = models.UUIDField(unique=True)
    src_facility_guid = models.UUIDField(null=True, blank=True)
    src_facility_name = models.CharField(max_length=100, null=True, blank=True)
    modality = models.CharField(max_length=10, null=True, blank=True)
    priority = models.CharField(max_length=10, null=True, blank=True)
    time_sent = models.DateTimeField(null=True, blank=True)
    time_received = models.DateTimeField(null=True, blank=True)
    time_reported = models.DateTimeField(null=True, blank=True)
    status = StatusField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'studies'

    def __unicode__(self):
        return self.guid.hex


class Feed(models.Model):
    guid = models.UUIDField(primary_key=True, default=uuid.uuid4)
    study_guid = models.UUIDField(unique=False, null=True)
    context = models.TextField(null=True, blank=True)
    action = models.CharField(max_length=50, null=True, blank=True)
    occurred_at = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'feeds'

    def __unicode__(self):
        return self.context

    @classmethod
    def store_activity(cls, guid, context, occurred_at, **kwargs):
        assert type(guid) == uuid.UUID or len(guid) in [32, 36]
        assert type(occurred_at) == datetime.datetime

        # Created at was supposed to populate with current datetime. But it was throwing Integrety error.
        # It could be related to this bug: https://code.djangoproject.com/ticket/17654
        feed = cls(guid=guid, context=context, occurred_at=occurred_at, action=kwargs.pop(u'action'),
                   study_guid=kwargs.pop(u'study_guid'), created_at=timezone.now(), **kwargs)
        feed.save()

    @classmethod
    def load_activity(cls, guid):
        assert type(guid) == uuid.UUID
        return Feed.objects.get(guid=guid)


class TurnAroundTime(models.Model):
    study_guid = models.UUIDField(unique=True)
    day = models.DateTimeField()
    turn_around_time = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    doctor_guid = models.UUIDField(null=True, blank=True)
    doctor_name = models.CharField(max_length=100, null=True, blank=True)
    src_facility_guid = models.UUIDField(null=True, blank=True)
    src_facility_name = models.CharField(max_length=100, null=True, blank=True)
    modality = models.CharField(max_length=10, null=True, blank=True)
    priority = models.CharField(max_length=10, null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'turn_around_time'

    def __unicode__(self):
        return self.study_guid.hex

