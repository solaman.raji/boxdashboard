from __future__ import unicode_literals

from django.apps import AppConfig


class BoxdashboardApiConfig(AppConfig):
    name = 'boxdashboard_api'
