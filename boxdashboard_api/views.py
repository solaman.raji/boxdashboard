# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import logging

import os
import requests
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

import boxdashboard.settings as settings
from boxdashboard_api.models import (
    Study as StudyModel,
    Feed as FeedModel,
    TurnAroundTime as TurnAroundTimeModel,
)
from boxdashboard_api.serializers import (
    StudyListSerializer,
    StudyCreateSerializer,
    StudyUpdateSerializer,
    FeedSerializer,
    TurnAroundTimeCreateSerializer,
    TurnAroundTimeListSerializer,
    ModalityListSerializer,
    PriorityListSerializer,
)

logger = logging.getLogger('boxdashboard')


class Study(APIView):
    def get(self, request):
        study = StudyModel.objects.filter(status=StudyModel.STATUS.active)
        serializer = StudyListSerializer(study, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = StudyCreateSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request, guid):
        data = {}

        try:
            study = StudyModel.objects.get(guid=guid)
        except ObjectDoesNotExist:
            logger.exception("update study >>> study guid %s does not exist", guid)
            data["error"] = True
            data["message"] = "study guid " + guid + " does not exist"
            return Response(data, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            logger.exception("update study >>> exception occurs in study fetch. reason: " + str(e))
            data["error"] = True
            data["message"] = "study guid " + guid + " does not exist"
            return Response(data, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

        if "time_reported" not in request.data:
            data["error"] = True
            data["message"] = "time_reported field is required"
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

        if "status" not in request.data:
            data["error"] = True
            data["message"] = "status field is required"
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

        study = self.update_object(study, request.data)
        serializer = StudyUpdateSerializer(study, data=request.data, partial=True)

        if serializer.is_valid():
            serializer.save()
            self.create_turn_around_time(study)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update_object(self, obj, data):
        if "time_reported" in data:
            obj.time_reported = data["time_reported"]
        if "status" in data:
            obj.status = data["status"]
        return obj

    def create_turn_around_time(self, obj):
        turn_around_time_data = {
            "study_guid": obj.guid,
            "day": obj.time_reported,
            "turn_around_time": self.get_turn_around_time(obj),
            "src_facility_guid": obj.src_facility_guid,
            "src_facility_name": obj.src_facility_name,
            "modality": obj.modality,
            "priority": obj.priority
        }

        turn_around_time_serializer = TurnAroundTimeCreateSerializer(data=turn_around_time_data)

        if turn_around_time_serializer.is_valid():
            turn_around_time_serializer.save()
            return True
        else:
            print turn_around_time_serializer.errors
            return False

    def get_turn_around_time(self, obj):
        time_diff = obj.time_reported - obj.time_sent
        return "{0:.2f}".format(time_diff.total_seconds())


class Feed(APIView):
    def get(self, request):
        logger.info('GET feed request')
        # FIXME: Decide on limit or add pagination
        feed = FeedModel.objects.order_by('-occurred_at').all()[:100]
        serializer = FeedSerializer(feed, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class StudyActivities(APIView):
    def get(self, request, guid, ascending=False):
        logger.info('GET Study activies for guid: {}'.format(guid))

        _order_by = 'occurred_at' if ascending else '-occurred_at'
        feed = FeedModel.objects.filter(study_guid=guid).order_by(_order_by).all()
        serializer = FeedSerializer(feed, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class TurnAroundTime(APIView):
    def get(self, request, timeline="week", filter_by=None):
        timeline = self.get_timeline(timeline)
        filter_by = self.get_filter_by(filter_by)

        query = "SELECT id, day, AVG(turn_around_time) as turn_around_time FROM turn_around_time WHERE day > datetime('now', '-" + str(timeline - 1) + " days') GROUP BY DATE(day)"
        turn_around_time_obj = TurnAroundTimeModel.objects.raw(query)

        serializer = TurnAroundTimeListSerializer(
            turn_around_time_obj,
            many=True,
            context={
                "timeline": timeline,
                "filter_by": filter_by
            }
        )

        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_timeline(self, timeline):
        if timeline == "week":
            timeline = 7
        elif timeline == "month":
            timeline = 30
        return timeline

    def get_filter_by(self, filter_by):
        if filter_by == "customer":
            filter_by = "src_facility_name"
        elif filter_by == "modality":
            filter_by = "modality"
        elif filter_by == "priority":
            filter_by = "priority"
        else:
            filter_by = None
        return filter_by


class Modality(APIView):
    def get(self, request):
        query = "SELECT id, modality, COUNT(id) as num_of_occurrence FROM studies GROUP BY modality"
        study = StudyModel.objects.raw(query)
        serializer = ModalityListSerializer(study, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class Priority(APIView):
    def get(self, request, timeline=None):
        query = self.get_query_string(timeline)
        study = StudyModel.objects.raw(query)
        serializer = PriorityListSerializer(study, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def get_query_string(self, timeline):
        query_select = 'SELECT id, priority, COUNT(id) as num_of_occurrence'
        query_from = 'FROM studies'
        query_where = ''
        query_group_by = 'GROUP BY priority'

        if timeline == "today":
            timeline = 1
            query_where = "WHERE created_at > datetime('now', '-" + str(timeline) + " days')"
        elif timeline == "week":
            timeline = 7
            query_where = "WHERE created_at > datetime('now', '-" + str(timeline) + " days')"
        elif timeline == "month":
            query_where = 'WHERE strftime("%m", datetime(created_at)) = strftime("%m", datetime("now"))'
        elif timeline == "year":
            query_where = 'WHERE strftime("%Y", datetime(created_at)) = strftime("%Y", datetime("now"))'

        query = query_select + ' ' + query_from + ' ' + query_where + '' + query_group_by
        return query


class Completion(APIView):
    def get(self, request):
        data = {}
        url = os.path.join(settings.HUB_SERVER, 'api', 'completion')
        logger.info("get completion statistics >>> url: " + str(url))

        try:
            response = requests.get(url)
            logger.info("get completion statistics >>> response: " + str(response))

            if response.status_code == 200:
                data = json.loads(response.text)
            else:
                data["error"] = True
                data["message"] = "Could not get completion statistics from hub."
                logger.error("get completion statistics >>> could not get completion statistics from hub")
        except Exception as e:
            data["error"] = True
            data["message"] = "Could not get completion statistics from hub."
            logger.exception("get completion statistics >>> exception occurs in api request. reason: " + str(e))

        return Response(data)


class HubStatus(APIView):
    # FIXME: Move this to settings
    HUB_STATUS_URI = '{}/api/online-status'.format(settings.HUB_SERVER)
    TIMEOUT_IN_SECONDS = 10.0
    ERROR_DICT = dict(
        hub_connected=False,
        error='Hub status API is returning error'
    )

    @classmethod
    def get_error_response_body(cls, message):
        body = dict(cls.ERROR_DICT)
        body.update({'error_response': '{}'.format(message)})
        return body

    @classmethod
    def get_status_from_hub(cls):
        response = requests.get(cls.HUB_STATUS_URI, timeout=cls.TIMEOUT_IN_SECONDS)
        response.raise_for_status()
        return response.status_code, response.json()

    def get(self, request):
        try:
            status_code, status_message = self.get_status_from_hub()
            return Response(data=status_message, status=status_code, content_type='application/json')
        except requests.HTTPError as ex:
            logger.error('Hub status API is throwing error. Exception: {}'.format(ex))
            error_message = self.get_error_response_body(ex.message)
            return Response(data=error_message,
                            status=200,
                            content_type='application/json')
        except requests.RequestException as ex:
            logger.error('Hub status API is not reachable. Exception: {}'.format(ex))
            # TODO: Decide on an error message on feedback
            error_message = self.get_error_response_body('')
            return Response(data=error_message,
                            status=200,
                            content_type='application/json')
