import httpretty
import requests
from django.test import Client
from django.test.testcases import TestCase

from boxdashboard_api.views import HubStatus


class TestHubStatusAPI(TestCase):
    def setUp(self):
        super(TestHubStatusAPI, self).setUp()
        self.client = Client(enforce_csrf_checks=False)

    def tearDown(self):
        self.client = None

    @httpretty.activate
    def test_status_if_hub_is_not_available(self):
        def request_callback(request, uri, headers):
            raise requests.Timeout('Connection timed out.')

        httpretty.HTTPretty.allow_net_connect = False
        httpretty.register_uri(httpretty.GET,
                               uri="http://localhost/api/online-status",
                               status=200,
                               body=request_callback)

        response = self.client.get('/dashboard-api/status/hub')

        expected_response_content = '''{
            "hub_connected" : false,
            "error": "Hub status API is returning error",
            "error_response": ""
        }'''
        assert response.status_code == 200
        assert response.content_type == 'application/json'
        self.assertJSONEqual(response.content, expected_response_content)

    @httpretty.activate
    def test_status_if_hub_returning_5xx_errors(self):
        httpretty.register_uri(httpretty.GET, "http://localhost/api/online-status",
                               body='Gateway timeout error',
                               status=502)
        response = self.client.get('/dashboard-api/status/hub', follow=False)

        expected_response_content = dict(
            hub_connected=False,
            error='Hub status API is returning error',
            error_response='502 Gateway timeout error'
        )

        assert response.status_code == 200
        assert response.content_type == 'application/json'

        actual_response = response.json()
        assert actual_response['hub_connected'] == expected_response_content['hub_connected']
        assert actual_response['error'] == expected_response_content['error']
        # FIXME: Decide on an error message
        assert "502" in expected_response_content['error_response']

    @httpretty.activate
    def test_status_if_hub_is_online(self):
        hub_response = '''{
    "hub_connected": true,
    "middleware_connected": true,
    "platform_connected": true,
    "storage": {
        "primary": {
             "total":250790436864,
             "used":211297779712,
             "free":39492657152
        },
        "media": {
            "total":10340831232,
            "used":1913417728,
            "free":8410636288
        }
    }
}'''
        httpretty.register_uri(httpretty.GET, "http://localhost/api/online-status",
                               body=hub_response, status=200,
                               content_type="application/json")
        response = self.client.get('/dashboard-api/status/hub')

        assert response.status_code == 200
        assert response.content_type == 'application/json'
        self.assertJSONEqual(response.content, hub_response)


class TestHubStatusProcessing(TestCase):
    @httpretty.activate
    def test_successful_status_from_hub(self):
        expected_response = '''{
    "hub_connected": true,
    "middleware_connected": true,
    "platform_connected": true,
    "storage": {
        "primary": {
             "total":250790436864,
             "used":211297779712,
             "free":39492657152
        },
        "media": {
            "total":10340831232,
            "used":1913417728,
            "free":8410636288
        }
    }
}'''
        httpretty.register_uri(httpretty.GET, "http://localhost/api/online-status",
                               body=expected_response, status=200,
                               content_type="application/json")

        status_code, actual_response = HubStatus.get_status_from_hub()
        assert status_code == 200
        import json
        self.assertJSONEqual(json.dumps(actual_response), expected_response)

    def test_bad_status_from_hub(self):
        pass

    def test_timeout_error_from_hub(self):
        pass
