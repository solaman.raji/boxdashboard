import json
import uuid

from django.test import TestCase
from django.utils import timezone
from rest_framework import status
from rest_framework.test import APIRequestFactory, APIClient

from boxdashboard_api.models import Study as StudyModel
from boxdashboard_api.views import Study as StudyView


class StudyViewPostTestCase(TestCase):
    study_id = 123
    guid = uuid.uuid4()
    src_facility_guid = uuid.uuid4()
    src_facility_name = "Crestview VI"
    modality = "CT"
    priority = "urgent"
    time_sent = timezone.now()
    time_received = timezone.now()
    time_reported = None

    data = {
        "study_id": study_id,
        "guid": str(guid),
        "src_facility_guid": str(src_facility_guid),
        "src_facility_name": src_facility_name,
        "modality": modality,
        "priority": priority,
        "time_sent": str(time_sent),
        "time_received": str(time_received),
        "time_reported": time_reported,
    }

    def test_study_post_with_client(self):
        client = APIClient()
        response = client.post('/dashboard-api/study', json.dumps(self.data), content_type='application/json')

        data = {
            "study_id": self.study_id,
            "guid": self.guid,
            "src_facility_guid": self.src_facility_guid,
        }

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, data)

    def test_study_post_with_request(self):
        factory = APIRequestFactory()
        request = factory.post('/dashboard-api/study', json.dumps(self.data), content_type='application/json')
        response = StudyView.as_view()(request)

        data = {
            "study_id": self.study_id,
            "guid": self.guid,
            "src_facility_guid": self.src_facility_guid,
        }

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, data)


class StudyViewPutTestCase(TestCase):
    study_id = 123
    guid = uuid.uuid4()
    src_facility_guid = uuid.uuid4()
    src_facility_name = "Crestview VI"
    modality = "CT"
    priority = "urgent"
    time_sent = timezone.now()
    time_received = timezone.now()
    time_reported = None
    status = StudyModel.STATUS.inactive

    def setUp(self):
        data = {
            "study_id": self.study_id,
            "guid": self.guid,
            "src_facility_guid": self.src_facility_guid,
            "src_facility_name": self.src_facility_name,
            "modality": self.modality,
            "priority": self.priority,
            "time_sent": self.time_sent,
            "time_received": self.time_received,
            "time_reported": self.time_reported,
        }

        StudyModel.objects.create(**data)

    def test_study_put_with_client(self):
        url = '/dashboard-api/study/' + str(self.guid.hex)
        data = {
            "status": self.status,
            "time_reported": str(timezone.now()),
        }

        client = APIClient()
        response = client.put(url, json.dumps(data), content_type="application/json")

        study = StudyModel.objects.get(guid=self.guid)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(study.status, StudyModel.STATUS.inactive)

    def test_study_put_with_request(self):
        url = '/dashboard-api/study'
        data = {
            "status": self.status,
            "time_reported": str(timezone.now()),
        }

        factory = APIRequestFactory()
        request = factory.put(url, json.dumps(data), content_type="application/json")
        response = StudyView.as_view()(request, str(self.guid))

        study = StudyModel.objects.get(guid=self.guid)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(study.status, StudyModel.STATUS.inactive)

    def test_study_update_with_wrong_guid_with_client(self):
        guid = uuid.uuid4()
        url = '/dashboard-api/study/' + str(guid.hex)
        data = {
            "status": self.status,
            "time_reported": str(timezone.now()),
        }

        client = APIClient()
        response = client.put(url, json.dumps(data), content_type="application/json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_study_update_with_wrong_guid_with_request(self):
        guid = uuid.uuid4()
        url = '/dashboard-api/study'
        data = {
            "status": self.status,
            "time_reported": str(timezone.now()),
        }

        factory = APIRequestFactory()
        request = factory.put(url, json.dumps(data), content_type="application/json")
        response = StudyView.as_view()(request, str(guid))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_study_put_empty_status_with_client(self):
        url = '/dashboard-api/study/' + str(self.guid.hex)
        data = {
            "status": "",
            "time_reported": str(timezone.now()),
        }

        client = APIClient()
        response = client.put(url, json.dumps(data), content_type="application/json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_study_put_empty_status_with_request(self):
        url = '/dashboard-api/study'
        data = {
            "status": "",
            "time_reported": str(timezone.now()),
        }

        factory = APIRequestFactory()
        request = factory.put(url, json.dumps(data), content_type="application/json")
        response = StudyView.as_view()(request, str(self.guid))

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_study_put_without_status_key_with_client(self):
        url = '/dashboard-api/study/' + str(self.guid.hex)
        data = {
            "time_reported": str(timezone.now()),
        }

        client = APIClient()
        response = client.put(url, json.dumps(data), content_type="application/json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_study_put_without_status_key_with_request(self):
        url = '/dashboard-api/study'
        data = {
            "time_reported": str(timezone.now()),
        }

        factory = APIRequestFactory()
        request = factory.put(url, json.dumps(data), content_type="application/json")
        response = StudyView.as_view()(request, str(self.guid))

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_study_put_empty_time_reported_with_client(self):
        url = '/dashboard-api/study/' + str(self.guid.hex)
        data = {
            "status": "inactive",
            "time_reported": "",
        }

        client = APIClient()
        response = client.put(url, json.dumps(data), content_type="application/json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_study_put_empty_time_reported_with_request(self):
        url = '/dashboard-api/study'
        data = {
            "status": "inactive",
            "time_reported": "",
        }

        factory = APIRequestFactory()
        request = factory.put(url, json.dumps(data), content_type="application/json")
        response = StudyView.as_view()(request, str(self.guid))

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_study_put_without_time_reported_key_with_client(self):
        url = '/dashboard-api/study/' + str(self.guid.hex)
        data = {
            "status": "inactive",
        }

        client = APIClient()
        response = client.put(url, json.dumps(data), content_type="application/json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_study_put_without_time_reported_key_with_request(self):
        url = '/dashboard-api/study'
        data = {
            "status": "inactive",
        }

        factory = APIRequestFactory()
        request = factory.put(url, json.dumps(data), content_type="application/json")
        response = StudyView.as_view()(request, str(self.guid))

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_study_put_update_object(self):
        study = StudyModel.objects.get(guid=self.guid)
        data = {
            "status": StudyModel.STATUS.inactive,
            "time_reported": str(timezone.now()),
        }
        study = StudyView.update_object(StudyView(), study, data)

        self.assertEqual(study.status, StudyModel.STATUS.inactive)
        self.assertEqual(study.time_reported, data["time_reported"])

    def test_study_put_update_object_with_request(self):
        study = StudyModel.objects.get(guid=self.guid)
        url = '/dashboard-api/study'
        data = {
            "status": StudyModel.STATUS.inactive,
            "time_reported": str(timezone.now()),
        }

        factory = APIRequestFactory()
        request = factory.put(url, json.dumps(data), content_type="application/json")
        request_data = json.loads(request.body)
        study = StudyView.update_object(StudyView(), study, request_data)

        self.assertEqual(study.status, StudyModel.STATUS.inactive)
        self.assertEqual(study.time_reported, request_data["time_reported"])

    def test_study_put_update_object_empty_status(self):
        study = StudyModel.objects.get(guid=self.guid)
        data = {
            "status": "",
            "time_reported": str(timezone.now()),
        }
        study = StudyView.update_object(StudyView(), study, data)

        self.assertEqual(study.status, "")
        self.assertEqual(study.time_reported, data["time_reported"])

    def test_study_put_update_object_empty_status_with_request(self):
        study = StudyModel.objects.get(guid=self.guid)
        url = '/dashboard-api/study'
        data = {
            "status": "",
            "time_reported": str(timezone.now()),
        }

        factory = APIRequestFactory()
        request = factory.put(url, json.dumps(data), content_type="application/json")
        request_data = json.loads(request.body)
        study = StudyView.update_object(StudyView(), study, request_data)

        self.assertEqual(study.status, "")
        self.assertEqual(study.time_reported, request_data["time_reported"])

    def test_study_put_update_object_without_status_key(self):
        study = StudyModel.objects.get(guid=self.guid)
        data = {
            "time_reported": str(timezone.now()),
        }
        study = StudyView.update_object(StudyView(), study, data)

        self.assertEqual(study.status, StudyModel.STATUS.active)
        self.assertEqual(study.time_reported, data["time_reported"])

    def test_study_put_update_object_without_status_key_with_request(self):
        study = StudyModel.objects.get(guid=self.guid)
        url = '/dashboard-api/study'
        data = {
            "time_reported": str(timezone.now()),
        }

        factory = APIRequestFactory()
        request = factory.put(url, json.dumps(data), content_type="application/json")
        request_data = json.loads(request.body)
        study = StudyView.update_object(StudyView(), study, request_data)

        self.assertEqual(study.status, StudyModel.STATUS.active)
        self.assertEqual(study.time_reported, request_data["time_reported"])

    def test_study_put_update_object_empty_time_reported(self):
        study = StudyModel.objects.get(guid=self.guid)
        data = {
            "status": StudyModel.STATUS.inactive,
            "time_reported": "",
        }
        study = StudyView.update_object(StudyView(), study, data)

        self.assertEqual(study.status, StudyModel.STATUS.inactive)
        self.assertEqual(study.time_reported, "")

    def test_study_put_update_object_empty_time_reported_with_request(self):
        study = StudyModel.objects.get(guid=self.guid)
        url = '/dashboard-api/study'
        data = {
            "status": StudyModel.STATUS.inactive,
            "time_reported": "",
        }

        factory = APIRequestFactory()
        request = factory.put(url, json.dumps(data), content_type="application/json")
        request_data = json.loads(request.body)
        study = StudyView.update_object(StudyView(), study, request_data)

        self.assertEqual(study.status, StudyModel.STATUS.inactive)
        self.assertEqual(study.time_reported, "")

    def test_study_put_update_object_without_time_reported_key(self):
        study = StudyModel.objects.get(guid=self.guid)
        data = {
            "status": StudyModel.STATUS.inactive,
        }
        study = StudyView.update_object(StudyView(), study, data)

        self.assertEqual(study.status, StudyModel.STATUS.inactive)
        self.assertEqual(study.time_reported, None)

    def test_study_put_update_object_without_time_reported_key_with_request(self):
        study = StudyModel.objects.get(guid=self.guid)
        url = '/dashboard-api/study'
        data = {
            "status": StudyModel.STATUS.inactive,
        }

        factory = APIRequestFactory()
        request = factory.put(url, json.dumps(data), content_type="application/json")
        request_data = json.loads(request.body)
        study = StudyView.update_object(StudyView(), study, request_data)

        self.assertEqual(study.status, StudyModel.STATUS.inactive)
        self.assertEqual(study.time_reported, None)
