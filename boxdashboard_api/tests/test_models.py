import uuid

from django.db import IntegrityError
from django.test import TestCase
from django.utils import timezone

from boxdashboard_api.models import Study as StudyModel
from boxdashboard_api.models import TurnAroundTime as TurnAroundTimeModel


class StudyModelTestCase(TestCase):
    pk = 1
    study_id = 123
    guid = uuid.uuid4()
    src_facility_guid = uuid.uuid4()
    src_facility_name = "Crestview VI"
    modality = "CT"
    priority = "urgent"
    time_sent = timezone.now()
    time_received = timezone.now()
    time_reported = None

    def setUp(self):
        data = {
            "study_id": self.study_id,
            "guid": self.guid,
            "src_facility_guid": self.src_facility_guid,
            "src_facility_name": self.src_facility_name,
            "modality": self.modality,
            "priority": self.priority,
            "time_sent": self.time_sent,
            "time_received": self.time_received,
            "time_reported": self.time_reported
        }

        StudyModel.objects.create(**data)

    def test_study_object_crate(self):
        study = StudyModel.objects.get(pk=self.pk)
        self.assertEqual(study.id, 1)
        self.assertEqual(study.study_id, self.study_id)
        self.assertEqual(study.guid, self.guid)

    def test_study_object_update_status_inactive(self):
        study = StudyModel.objects.get(guid=self.guid)
        study.status = StudyModel.STATUS.inactive
        study.save()
        self.assertEqual(study.status, StudyModel.STATUS.inactive)


class TurnAroundTimeModelTestCase(TestCase):
    pk = 1
    study_guid = uuid.uuid4()
    day = timezone.now()

    def setUp(self):
        data = {
            "study_guid": self.study_guid,
            "day": self.day
        }

        TurnAroundTimeModel.objects.create(**data)

    def test_fetch(self):
        turn_around_time = TurnAroundTimeModel.objects.get(pk=self.pk)
        self.assertEqual(turn_around_time.id, self.pk)
        self.assertEqual(turn_around_time.study_guid, self.study_guid)
        self.assertEqual(turn_around_time.day, self.day)

    def test_uniqueness_of_study_guid(self):
        with self.assertRaises(IntegrityError):
            TurnAroundTimeModel.objects.create(study_guid=self.study_guid, day=self.day)

    def test_not_null_constraint(self):
        with self.assertRaises(IntegrityError):
            TurnAroundTimeModel.objects.create()

    def test_not_null_constraint_of_study_guid(self):
        with self.assertRaises(IntegrityError):
            TurnAroundTimeModel.objects.create(day=self.day)

    def test_not_null_constraint_of_day(self):
        with self.assertRaises(IntegrityError):
            TurnAroundTimeModel.objects.create(study_guid=uuid.uuid4())
