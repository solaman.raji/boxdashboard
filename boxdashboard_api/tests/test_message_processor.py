from django.test import TestCase

from boxdashboard_api.message_consumer import MessageProcessor


class TestStudyMessageConsumer(TestCase):
    def test_consume_study_created(self):
        study_creation_message = '''{  
           "action":"order-created",
           "message_guid":"f33e04ef-2c1d-4bbf-9894-52b23627ad46",
           "occurred_at":"2017-11-14T12:32:06.008964+00:00",
           "context":"Study 4 of patient ANWOJU^TIMILEHIN from Test Box is created",
           "study_guid": "e55e04ef-2c1d-4bbf-9894-52b23627cd11"
        }'''

        assert MessageProcessor.store_activity(study_creation_message)

    def test_consume_study_downloaded(self):
        study_downloaded_message = '''{  
           "action":"dicoms-downloaded",
           "message_guid":"eb99cd79-38d5-4dab-bd5f-22c87f36545e",
           "occurred_at":"2017-11-14T12:32:41.479311+00:00",
           "context":"Study 4 DICOMs of patient ANWOJU^TIMILEHIN are downloaded from Test Box",
           "study_guid": "e55e04ef-2c1d-4bbf-9894-52b23627cd11"
        }'''

        assert MessageProcessor.store_activity(study_downloaded_message)


class TestStudyReportedMessageConsumer(TestCase):
    def test_consume_study_reported(self):
        study_reported_message = '''{  
           "action":"reported",
           "message_guid":"e10770f9-becc-4bbe-b166-7a9004ed5c3f",
           "occurred_at":"2017-11-14T12:34:06.476678+00:00",
           "context":"Study #4 has been reported",
           "study_guid": "e55e04ef-2c1d-4bbf-9894-52b23627cd11"
        }'''

        assert MessageProcessor.store_activity(study_reported_message)
