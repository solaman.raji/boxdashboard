import uuid

from django.test import TestCase
from django.utils import timezone

from boxdashboard_api.models import Feed


class ActivityTestCase(TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    # FIXME: Use freeze
    def test_store_activity(self):
        guid = uuid.uuid4()
        study_guid = uuid.uuid4()
        occurred_at = timezone.now()

        activity_dict = dict(
            guid=guid,
            context=u'Dr. Fatade has reported Study #312',
            action='reported',
            occurred_at=occurred_at,
            study_guid=study_guid.hex
        )

        Feed.store_activity(**activity_dict)

        actual_activity = Feed.objects.get(guid=guid)
        assert actual_activity.guid == guid
        assert actual_activity.context == u'Dr. Fatade has reported Study #312'
        assert actual_activity.occurred_at == occurred_at
        assert actual_activity.action == 'reported'
        assert actual_activity.study_guid == study_guid

    def test_store_failed_if_already_exists(self):
        pass

    def test_load_activity(self):
        activity = dict(
            guid=uuid.uuid4(),
            context=u'Dr. Fatade has reported Study #312',
            action='reported',
            occurred_at=timezone.now(),
            study_guid=uuid.uuid4()
        )
        Feed.objects.create(**activity)

        feed_loaded = Feed.load_activity(activity['guid'])

        assert feed_loaded.guid == activity['guid']
        assert feed_loaded.context == activity['context']
        assert feed_loaded.occurred_at == activity['occurred_at']
        assert feed_loaded.action == activity['action']
        assert feed_loaded.study_guid == activity['study_guid']

    def test_raise_object_not_found_if_does_not_exists(self):
        pass
