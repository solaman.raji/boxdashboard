from django.conf.urls import url

from boxdashboard_api.views import (
    Study,
    Feed,
    StudyActivities,
    TurnAroundTime,
    Modality,
    Priority,
    Completion,
    HubStatus,
)

urlpatterns = [
    url(r'^dashboard-api/study/?$', Study.as_view()),
    url(r'^dashboard-api/study/(?P<guid>\w+)$', Study.as_view()),
    url(r'^dashboard-api/feed/?$', Feed.as_view()),
    url(r'^dashboard-api/study/(?P<guid>\w+)/activities/?$', StudyActivities.as_view()),
    url(r'^dashboard-api/turn_around_time/?$', TurnAroundTime.as_view()),
    url(r'^dashboard-api/turn_around_time/(?P<timeline>\w+)/$', TurnAroundTime.as_view()),
    url(r'^dashboard-api/turn_around_time/(?P<timeline>\w+)/(?P<filter_by>\w+)/$', TurnAroundTime.as_view()),
    url(r'^dashboard-api/modality/?$', Modality.as_view()),
    url(r'^dashboard-api/priority/?$', Priority.as_view()),
    url(r'^dashboard-api/priority/(?P<timeline>\w+)/$', Priority.as_view()),
    url(r'^dashboard-api/completion/?$', Completion.as_view()),

    url(r'^dashboard-api/status/hub$', HubStatus.as_view())
]
