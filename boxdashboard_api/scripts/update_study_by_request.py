import json

from django.utils import timezone
from rest_framework import status
from rest_framework.test import APIRequestFactory

from boxdashboard_api.models import Study as StudyModel
from boxdashboard_api.views import Study as StudyView


# Update Study
def update_study():
    studies = StudyModel.objects.all()

    for study in studies:
        guid = study.guid.hex
        time_reported = study.time_received + timezone.timedelta(hours=3)

        data = {
            "status": StudyModel.STATUS.inactive,
            "time_reported": str(time_reported)
        }

        factory = APIRequestFactory()
        request = factory.put('/dashboard-api/study/', json.dumps(data), content_type='application/json')
        response = StudyView.as_view()(request, guid)

        assert response.status_code == status.HTTP_201_CREATED


def run():
    update_study()
