import random
import uuid

from django.utils import timezone

from boxdashboard_api.models import TurnAroundTime as TurnAroundTimeModel


# Create Turn Around Time
def create_turn_around_time(num_of_object, num_of_different_hours):
    src_facility_name_list = ['Crestview VI', 'Crestview NOHIL', 'Afriglobal', 'Shinozada']
    modality_list = ['X Ray', 'MRI', 'CT Scan', 'HSG', 'MG', 'Ultrasound']
    priority_list = ['urgent', 'routine']
    doctor_name_list = ['Doctor01', 'Doctor02', 'Doctor03']

    for day_delta in range(num_of_object):
        for hour_delta in range(num_of_different_hours):
            study_guid = uuid.uuid4()
            day = timezone.now() - timezone.timedelta(days=day_delta, hours=hour_delta)
            turn_around_time = random.randint(1, 9999)
            doctor_guid = uuid.uuid4()
            doctor_name = random.choice(doctor_name_list)
            src_facility_guid = uuid.uuid4()
            src_facility_name = random.choice(src_facility_name_list)
            modality = random.choice(modality_list)
            priority = random.choice(priority_list)

            data = {
                "study_guid": study_guid,
                "day": day,
                "turn_around_time": turn_around_time,
                "doctor_guid": doctor_guid,
                "doctor_name": doctor_name,
                "src_facility_guid": src_facility_guid,
                "src_facility_name": src_facility_name,
                "modality": modality,
                "priority": priority
            }

            TurnAroundTimeModel.objects.create(**data)


def run():
    num_of_object = 10
    num_of_different_hours = 10
    create_turn_around_time(num_of_object, num_of_different_hours)
