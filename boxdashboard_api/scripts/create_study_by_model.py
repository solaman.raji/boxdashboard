import random
import uuid

from django.utils import timezone

from boxdashboard_api.models import Study as StudyModel


def check_study_id_exist(study_id):
    try:
        StudyModel.objects.get(study_id=study_id)
    except Exception:
        return False

    print "Study ID: " + str(study_id) + " exists"
    return True


def get_unique_study_id():
    study_id = random.randint(1, 99999)
    print "Study ID: " + str(study_id)

    if check_study_id_exist(study_id):
        get_unique_study_id()
    else:
        return study_id


# Create Study
def create_study(num_of_object, num_of_hours, script_type):
    priority_list = ['urgent', 'routine']
    modality_list = ['X Ray', 'MRI', 'CT Scan', 'HSG', 'MG', 'Ultrasound']
    src_facility_name_list = ['Crestview VI', 'Crestview NOHIL', 'Afriglobal', 'Shinozada']

    created_at_field = StudyModel._meta.get_field("created_at")
    created_at_field.auto_now_add = False

    for day_delta in range(num_of_object):
        for hour_delta in range(num_of_hours):
            # study_id = get_unique_study_id()
            study_id = random.randint(1, 99999)
            guid = uuid.uuid4()
            src_facility_guid = uuid.uuid4()
            src_facility_name = random.choice(src_facility_name_list)
            modality = random.choice(modality_list)
            priority = random.choice(priority_list)
            time_sent = timezone.now() - timezone.timedelta(days=day_delta, hours=hour_delta)
            time_received = timezone.now() - timezone.timedelta(days=day_delta, hours=hour_delta)
            time_reported = None
            created_at = timezone.now() - timezone.timedelta(days=day_delta, hours=hour_delta)

            data = {
                "study_id": study_id,
                "guid": guid,
                "src_facility_guid": src_facility_guid,
                "src_facility_name": src_facility_name,
                "modality": modality,
                "priority": priority,
                "time_sent": time_sent,
                "time_received": time_received,
                "time_reported": time_reported,
                "created_at": created_at,
            }

            try:
                StudyModel.objects.create(**data)
            except Exception as e:
                print "Exception occurs for study_id " + str(study_id) + " Reason: " + str(e)


def run(*args):
    num_of_object = 10
    num_of_hours = 10
    script_type = "generate"

    if len(args) > 0 and args[0] is not None:
        num_of_object = int(args[0])

    if len(args) > 1 and args[1] is not None:
        num_of_hours = int(args[1])

    if len(args) > 2 and args[2] is not None:
        script_type = args[2]

    create_study(num_of_object, num_of_hours, script_type=script_type)
