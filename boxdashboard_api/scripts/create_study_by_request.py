import json
import random
import uuid

from django.utils import timezone
from rest_framework import status
from rest_framework.test import APIRequestFactory

from boxdashboard_api.views import Study as StudyView


# Create Study
def create_study(num_of_object, num_of_different_hours):
    priority_list = ['urgent', 'routine']
    modality_list = ['X Ray', 'MRI', 'CT Scan', 'HSG', 'MG', 'Ultrasound']
    src_facility_name_list = ['Crestview VI', 'Crestview NOHIL', 'Afriglobal', 'Shinozada']

    for day_delta in range(num_of_object):
        for hour_delta in range(num_of_different_hours):
            study_id = random.randint(1, 99999)
            guid = uuid.uuid4()
            src_facility_guid = uuid.uuid4()
            src_facility_name = random.choice(src_facility_name_list)
            modality = random.choice(modality_list)
            priority = random.choice(priority_list)
            time_sent = timezone.now() - timezone.timedelta(days=day_delta, hours=hour_delta)
            time_received = timezone.now() - timezone.timedelta(days=day_delta, hours=hour_delta)
            time_reported = None

            data = {
                "study_id": study_id,
                "guid": str(guid),
                "src_facility_guid": str(src_facility_guid),
                "src_facility_name": src_facility_name,
                "modality": modality,
                "priority": priority,
                "time_sent": str(time_sent),
                "time_received": str(time_received),
                "time_reported": time_reported
            }

            factory = APIRequestFactory()
            request = factory.post('/dashboard-api/study/', json.dumps(data), content_type='application/json')
            response = StudyView.as_view()(request)

            assert response.status_code == status.HTTP_201_CREATED


def run():
    num_of_object = 10
    num_of_different_hours = 10
    create_study(num_of_object, num_of_different_hours)
