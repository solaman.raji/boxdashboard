# -*- coding: utf-8 -*-
import decimal
from collections import defaultdict
from datetime import datetime

import pytz
from django.utils import timezone
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from boxdashboard_api.models import (
    Study as StudyModel,
    TurnAroundTime as TurnAroundTimeModel
)


class StudyCreateSerializer(serializers.Serializer):
    study_id = serializers.IntegerField(
        required=True,
        validators=[UniqueValidator(queryset=StudyModel.objects.all())]
    )
    guid = serializers.UUIDField(
        required=True,
        validators=[UniqueValidator(queryset=StudyModel.objects.all())]
    )
    src_facility_guid = serializers.UUIDField(required=False, allow_null=True)
    src_facility_name = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    modality = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    priority = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    time_sent = serializers.DateTimeField(required=True)
    time_received = serializers.DateTimeField(required=True)
    time_reported = serializers.DateTimeField(required=False, allow_null=True)
    status = serializers.CharField(required=False)

    def create(self, validated_data):
        return StudyModel.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.time_reported = validated_data.get("time_reported", instance.time_reported)
        instance.status = validated_data.get("status", instance.status)
        instance.save()
        return instance

    def to_representation(self, obj):
        return {
            "study_id": obj.study_id,
            "guid": obj.guid,
            "src_facility_guid": obj.src_facility_guid,
        }


class StudyUpdateSerializer(serializers.Serializer):
    time_reported = serializers.DateTimeField(required=True, allow_null=False)
    status = serializers.CharField(required=True, allow_null=False, allow_blank=False)

    def update(self, instance, validated_data):
        instance.time_reported = validated_data.get("time_reported", instance.time_reported)
        instance.status = validated_data.get("status", instance.status)
        instance.save()
        return instance


class StudyListSerializer(serializers.Serializer):
    study_id = serializers.IntegerField(required=True)
    guid = serializers.UUIDField(required=True)
    src_facility_name = serializers.CharField(required=True)
    modality = serializers.CharField(required=True)
    priority = serializers.CharField(required=True)
    time_sent = serializers.DateTimeField(required=True)
    time_received = serializers.DateTimeField(required=True)
    time_reported = serializers.DateTimeField(required=True)
    status = serializers.CharField(required=True)


class FeedSerializer(serializers.Serializer):
    action = serializers.CharField(read_only=True)
    context = serializers.CharField(read_only=True)
    occurred_at = serializers.DateTimeField(read_only=True)


class TurnAroundTimeCreateSerializer(serializers.Serializer):
    study_guid = serializers.UUIDField(
        required=True,
        validators=[UniqueValidator(queryset=TurnAroundTimeModel.objects.all())]
    )
    day = serializers.DateTimeField(required=True)
    turn_around_time = serializers.DecimalField(required=True, max_digits=10, decimal_places=2)
    doctor_guid = serializers.UUIDField(required=False, allow_null=True)
    doctor_name = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    src_facility_guid = serializers.UUIDField(required=False, allow_null=True)
    src_facility_name = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    modality = serializers.CharField(required=False, allow_null=True, allow_blank=True)
    priority = serializers.CharField(required=False, allow_null=True, allow_blank=True)

    def create(self, validated_data):
        return TurnAroundTimeModel.objects.create(**validated_data)


class TurnAroundTimeCustomListSerializer(serializers.ListSerializer):

    @property
    def data(self):
        timeline = self.context.get("timeline")
        filter_by = self.context.get("filter_by")
        serialized_data = super(TurnAroundTimeCustomListSerializer, self).data

        if filter_by is not None:
            data_dict = self.get_data_with_filter(serialized_data, timeline, filter_by)
        else:
            data_dict = self.get_data_without_filter(serialized_data, timeline)

        return data_dict

    def get_data_with_filter(self, serialized_data, timeline, filter_by):
        data = defaultdict(list)
        sr_data_dict = defaultdict(list)

        for sr_data in serialized_data:
            filter_by_name = sr_data[filter_by]
            sr_data = self.format_serializer_data(date_exist=True, data=sr_data)
            sr_data_dict[filter_by_name].append(sr_data)

        for filter_by_name, tat_obj_list in sr_data_dict.iteritems():
            date_list = self.get_date_list(timeline)

            for tat_obj in tat_obj_list:
                date_obj = self.generate_date_from_datetime_str(tat_obj["day"])

                if date_obj in date_list:
                    data[filter_by_name].append(tat_obj)
                    date_list.remove(date_obj)

            for date_obj in date_list:
                sr_data = self.format_serializer_data(date_exist=False, date_obj=date_obj)
                data[filter_by_name].append(sr_data)

        return data

    def get_data_without_filter(self, serialized_data, timeline):
        data = {}
        data_list = []
        date_list = self.get_date_list(timeline)

        for sr_data in serialized_data:
            date_obj = self.generate_date_from_datetime_str(sr_data['day'])

            if date_obj in date_list:
                sr_data = self.format_serializer_data(date_exist=True, data=sr_data)
                data_list.append(sr_data)
                date_list.remove(date_obj)

        for date_obj in date_list:
            sr_data = self.format_serializer_data(date_exist=False, date_obj=date_obj)
            data_list.append(sr_data)

        data["turn_around_time"] = data_list
        return data

    def get_date_list(self, timeline):
        date_list = []
        for day in range(timeline):
            date_obj = timezone.now() - timezone.timedelta(days=day)
            date_list.append(date_obj.date())
        return date_list

    def generate_datetime_from_date(self, date_obj):
        return timezone.datetime(date_obj.year, date_obj.month, date_obj.day, hour=0, minute=0, second=0,
                                 tzinfo=pytz.UTC)

    def generate_date_from_datetime_str(self, datetime_str):
        return datetime.strptime(datetime_str, '%Y-%m-%dT%H:%M:%S.%fZ').date()

    def format_serializer_data(self, date_exist, date_obj=None, data=None):
        formatted_data = {}
        if date_exist:
            formatted_data['day'] = data['day']
            formatted_data['turn_around_time'] = data['turn_around_time']
        else:
            formatted_data['day'] = self.generate_datetime_from_date(date_obj)
            formatted_data['turn_around_time'] = "0.00"
        return formatted_data

    def get_formatted_data(self, data):
        data_list = []
        for key, value in data.iteritems():
            data_dict = {}
            data_dict[key] = value
            data_list.append(data_dict)
        return data_list


class TurnAroundTimeListSerializer(serializers.Serializer):
    day = serializers.DateTimeField(read_only=True)
    turn_around_time = serializers.SerializerMethodField(read_only=True)
    src_facility_name = serializers.CharField(read_only=True)
    modality = serializers.CharField(read_only=True)
    priority = serializers.CharField(read_only=True)

    def get_turn_around_time(self, obj):
        return "{0:.2f}".format(decimal.Decimal(obj.turn_around_time) / decimal.Decimal('60'))

    class Meta:
        list_serializer_class = TurnAroundTimeCustomListSerializer


class ModalityListSerializer(serializers.Serializer):
    def to_representation(self, obj):
        return {
            "modality": obj.modality,
            "num_of_occurrence": str(obj.num_of_occurrence)
        }


class PriorityListSerializer(serializers.Serializer):
    def to_representation(self, obj):
        return {
            "priority": obj.priority,
            "num_of_occurrence": str(obj.num_of_occurrence)
        }
