import json
import logging


from boxdashboard_api.models import Feed
from django.utils import dateparse

logger = logging.getLogger(__name__)

'''
# Sample message
{"message_guid": "e55944c6-a55d-422d-bac1-43d2dfae7d2e", 
 "context": "Dr. Fatade has reported Study #312",
 "occurred_at": "2017-11-05T10:19:05.83+00:00",
 "study_guid": "e55e04ef-2c1d-4bbf-9894-52b23627cd11",
 "action": "reported"}



{"message_guid":"54a9c5bf-10e0-4f85-902a-478525f4dcc1",
 "context": "Dr. Fatade has reported Study #313",
 "occurred_at": "2017-11-05T10:19:05.83+00:00",
 "study_guid": "e55e04ef-2c1d-4bbf-9894-52b23627cd11",
 "action": "reported"}

{"guid":"0881b52d-3b1e-46b4-a70d-bf245880d3a7",
 "context": "Dr. Fatade has reported Study #444",
 "occurred_at": "2017-11-05T10:19:05.83+00:00",
 "study_guid": "e55e04ef-2c1d-4bbf-9894-52b23627cd11",
 "action": "reported"}

{"guid":"c1aeae16-3e65-4a45-b4fe-9cffad4d8f61",
 "context": "Dr. Fatade has reported Study #555",
 "occurred_at": "2017-11-05T10:19:05.83+00:00",
 "study_guid": "e55e04ef-2c1d-4bbf-9894-52b23627cd11",
 "action": "reported"}

'''


# TODO: Is this a generic processor?
class MessageProcessor(object):
    def __init__(self):
        super(MessageProcessor, self).__init__()

    @classmethod
    def store_activity(cls, activity_message_str):
        logger.info('Message received: {}'.format(activity_message_str))

        message_dict = json.loads(activity_message_str)
        occurred_at = message_dict.get('occurred_at', '')
        message_dict['occurred_at'] = dateparse.parse_datetime(occurred_at)
        message_dict['guid'] = message_dict.pop('message_guid')

        Feed.store_activity(**message_dict)

        return True
