Deploy projects using ansible
==============================

This document is to explain how to run ansible commands to deploy dashboard to a host

Install virtualenv and virtualenv-wrapper before installing ansible
::
    sudo apt install -y python-pip
    sudo pip install -U pip
    sudo pip install virtualenv virtualenvwrapper


Activate a virtualenv that contains ansible or install it
::
    mkvirtualenv virtualenv-deployment
    pip install ansible

Enable virtualenv and change directory to project root
::
    workon virtualenv-deployment
    cd <boxdashboard-dir>

    export ANSIBLE_HOST_KEY_CHECKING=False
    export ANSIBLE_CONFIG=ansible/playbooks/ansible.cfg
    export ANSIBLE_FORCE_COLOR=true


Check if ansible can access the vagrant
::
    ansible vagrant-01 -i ansible/playbooks/hosts -m ping

Run ansible playbook to deploy. (Comment out devserver-01 and devserver-02 in hosts file.)
::
    ansible-playbook --limit vagrant-01 -i ansible/playbooks/hosts  ansible/playbooks/box-dashboard.yml  -v

.. Notes:
    SSH to vagrant guest may fail if vagrant is destroyed. To avoid ssh hostkey checking for vagrant guest please do
any of the following.

    export ANSIBLE_HOST_KEY_CHECKING=False

    ssh-keygen -R [127.0.0.1]:2222


Frontend build
---------------

For convenience, frontend project should be downloaded under the same parent as boxdashboard.

Build the project inside vagrant
::
    vagrant ssh
    cd /box-frontend
    . ah-build.sh

Deploy the project from your machine
::
    cd <box-frontend-project-dir>
    workon virtualenv-deployment

    export ANSIBLE_HOST_KEY_CHECKING=False
    export PLAYBOOKS_HOME=../boxdashboard/ansible/playbooks
    export ANSIBLE_CONFIG=$PLAYBOOKS_HOME/ansible.cfg

    export PLAYBOOK=$PLAYBOOKS_HOME/box-dashboard-frontend.yml
    export ANSIBLE_HOSTS_FILE=$PLAYBOOKS_HOME/hosts

    ansible-playbook -i $ANSIBLE_HOSTS_FILE $PLAYBOOK -v
