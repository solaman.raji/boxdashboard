from __future__ import unicode_literals

SECRET_KEY = "b495a05c396843b6b47ac944a72c92ed"
NEVERCACHE_KEY = "b5d87bb4e17c483093296fa321056bdc"
ALLOWED_HOSTS = [
    '192.168.11.13',
    '127.0.0.1',
    'testserver',
    'localhost',
    'dev-box-dashboard-01.cloud.alemhealth.com',
    'dev-box-dashboard-02.cloud.alemhealth.com',
    'test-hub-dashboard-01.cloud.alemhealth.com',
    'dev.13.127.4.121.xip.io',
    'www.13.126.141.238.xip.io',
    '192.168.11.13',
]

STATIC_URL = '/boxdashboard/static/'
STATIC_ROOT = "/var/www/dashboard-static/"

SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTOCOL", "https")

CACHE_MIDDLEWARE_SECONDS = 60

CACHE_MIDDLEWARE_KEY_PREFIX = "boxdashboard"

SESSION_ENGINE = "django.contrib.sessions.backends.cache"

HUB_SERVER = "http://localhost"
